# Copyright 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=festvox tag=v${PV} ]
require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

SUMMARY="A small, fast run-time speech synthesis engine"
DESCRIPTION="
It's primarily designed for small embedded machines and/or large servers.
Flite is designed as an alternative synthesis engine to Festival for voices
built using the FestVox suite of voice building tools.
"
HOMEPAGE+=" http://cmuflite.org"

LICENCES="
    BSD-3 [[ note = [ src/cg/cst_*, see COPYING ] ]]
    BSD-4
"
SLOT="0"
MYOPTIONS="
    alsa
    pulseaudio

    ( alsa pulseaudio ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        alsa? ( sys-sound/alsa-lib )
        pulseaudio? ( media-sound/pulseaudio )
"

# parallel builds are broken with make 4.4
# https://github.com/festvox/flite/issues/86
DEFAULT_SRC_COMPILE_PARAMS=( -j1 )

DEFAULT_SRC_CONFIGURE_PARAMS+=(
    --enable-shared
)
DEFAULT_SRC_CONFIGURE_OPTIONS+=(
    'alsa --with-audio=alsa'
    'pulseaudio --with-audio=pulseaudio'
)

