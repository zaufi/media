# Copyright 2009 Michael Forney
# Copyright 2011-2015 Pierre Lejeune <superheron@gmail.com>
# Copyright 2014 Johannes Nixdorf <mixi@exherbo.org>
# Copyright 2019 Ridai Govinda Pombo <ridai.govinda@gmail.com>
# Distributed under the terms of the GNU General Public License v2

MY_PV=${PV/b}

require cmake \
    flag-o-matic \
    freedesktop-desktop \
    python [ blacklist="2 3.8 3.9" multibuild=false ] \
    ffmpeg [ with_opt=true ]

export_exlib_phases src_prepare src_configure src_install

SUMMARY="Free and open source 3D creation suite"
HOMEPAGE="https://www.blender.org"
DOWNLOADS="https://download.blender.org/source/${PNV}.tar.xz"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    ffmpeg
    fftw     [[ description = [ Support for smoke, audio effects and the ocean simulator ] ]]
    jack     [[ description = [ Support for sound output through jack ] ]]
    jpeg2000
    openal   [[ description = [ Support for sound output using openal ] ]]
    openmp
    sdl      [[ description = [ Support for sound output using SDL and joystick support ] ]]
    sndfile  [[ description = [ Support for some audio codecs through libsndfile ] ]]
    wayland
    webp

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

# FIXME: Tests fire tons of sydbox violations
RESTRICT="test"

DEPENDENCIES="
    build:
        sci-libs/eigen:3[>=3.2.7]
        x11-proto/xorgproto
        wayland? ( sys-libs/wayland-protocols[>=1.15] )
    build+run:
        app-arch/lzo:2
        app-arch/zstd
        dev-libs/boost[>=1.68.0]
        dev-libs/gmp:=
        dev-libs/libepoxy
        dev-libs/libglvnd
        dev-libs/onetbb
        dev-libs/pugixml
        dev-python/numpy[>=1.7.0][python_abis:*(-)?]
        media-libs/OpenColorIO[>=2.0]
        media-libs/OpenImageIO[>=2.1.12]
        media-libs/OpenSubdiv[>=3.4]
        media-libs/freetype:2[woff2]
        media-libs/imath
        media-libs/libpng:=
        media-libs/openexr[>=3]
        media-libs/tiff:=
        sys-libs/zlib
        x11-libs/libX11
        x11-libs/libXfixes
        x11-libs/libXi
        x11-libs/libXrender
        x11-libs/libXxf86vm
        fftw? ( sci-libs/fftw[>=3.0] )
        jack? ( media-sound/jack-audio-connection-kit )
        jpeg2000? ( media-libs/OpenJPEG:2 )
        openal? ( media-libs/openal )
        openmp? ( sys-libs/libgomp:= )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        sdl? ( media-libs/SDL:2 )
        sndfile? ( media-libs/libsndfile )
        wayland? (
            sys-apps/dbus
            sys-libs/libdecor[>=0.1]
            sys-libs/wayland[>=1.12]
            x11-libs/libxkbcommon[wayland?]
        )
        webp? ( media-libs/libwebp:= )
    suggestion:
        dev-python/requests[python_abis:*(-)?] [[ description = [ Support to upload models to sketchfab ] ]]
"

blender_src_prepare() {
    if option openmp; then
        # stolen from the cmake test
        # we can't just test for the flag because clang ignores it and returns 0
        cat > "${WORKBASE}"/openmp-test.c <<HERE
#include <omp.h>
int main() {
    #ifdef _OPENMP
        return 0;
    #else
        breaks_on_purpose
    #endif
}
HERE

        if ! ${CC}  ${CFLAGS}   -fopenmp "${WORKBASE}"/openmp-test.c -o /dev/null &> /dev/null ||
           ! ${CXX} ${CXXFLAGS} -fopenmp "${WORKBASE}"/openmp-test.c -o /dev/null &> /dev/null; then
            eerror "You enabled the openmp option for blender but your selected compiler doesn't support it."
            eerror "Either use a different compiler or disable the openmp option."
            die
        fi
    fi

    cmake_src_prepare
}

blender_src_configure() {
    # WITH_OPENIMAGEIO: required for cycles
    # WITH_INSTALL_PORTABLE: respect CMAKE_INSTALL_PREFIX
    # WITH_PYTHON_INSTALL*: use the system python modules
    # WITH_INPUT_NDOF: unwritten dependency
    # WITH_SYSTEM_BULLET: not supported by upstream because of missing features
    # WITH_AUDASPACE: temporarily hard-disabled to due link failure, last checked: 2.93.5
    # WITH_PULSEAUDIO: depends on WITH_AUDASPACE
    local args=(
        -DCMAKE_BUILD_TYPE:STRING=Release
        -DCMAKE_SKIP_BUILD_RPATH:BOOL=TRUE
        -DPYTHON_VERSION:STRING=$(python_get_abi)
        -DWITH_ALEMBIC:BOOL=FALSE
        -DWITH_AUDASPACE:BOOL=FALSE
        -DWITH_BLENDER:BOOL=TRUE
        -DWITH_BLENDER_THUMBNAILER:BOOL=TRUE
        -DWITH_BOOST:BOOL=TRUE
        -DWITH_BUILDINFO:BOOL=FALSE
        -DWITH_BULLET:BOOL=TRUE
        -DWITH_CLANG:BOOL=FALSE
        -DWITH_CODEC_AVI:BOOL=TRUE
        -DWITH_COMPILER_CCACHE:BOOL=FALSE
        -DWITH_COMPOSITOR_CPU:BOOL=TRUE
        -DWITH_COMPOSITOR_REALTIME_TESTS:BOOL=FALSE
        -DWITH_CUDA_DYNLOAD:BOOL=TRUE
        -DWITH_CYCLES:BOOL=TRUE
        -DWITH_CYCLES_CUDA_BINARIES:BOOL=FALSE
        -DWITH_CYCLES_CUDA_BUILD_SERIAL:BOOL=FALSE
        -DWITH_CYCLES_DEBUG:BOOL=FALSE
        -DWITH_CYCLES_DEVICE_CUDA:BOOL=TRUE
        -DWITH_CYCLES_DEVICE_HIP:BOOL=FALSE
        -DWITH_CYCLES_DEVICE_ONEAPI:BOOL=FALSE
        -DWITH_CYCLES_DEVICE_OPTIX:BOOL=FALSE
        -DWITH_CYCLES_EMBREE:BOOL=FALSE
        -DWITH_CYCLES_HYDRA_RENDER_DELEGATE:BOOL=FALSE
        -DWITH_CYCLES_KERNEL_ASAN:BOOL=FALSE
        -DWITH_CYCLES_LOGGING:BOOL=TRUE
        -DWITH_CYCLES_NATIVE_ONLY:BOOL=FALSE
        -DWITH_CYCLES_ONEAPI_BINARIES:BOOL=FALSE
        -DWITH_CYCLES_ONEAPI_HOST_TASK_EXECUTION:BOOL=FALSE
        -DWITH_CYCLES_OSL:BOOL=FALSE
        -DWITH_CYCLES_PATH_GUIDING:BOOL=FALSE
        -DWITH_CYCLES_PRECOMPUTE:BOOL=FALSE
        -DWITH_CYCLES_STANDALONE:BOOL=FALSE
        -DWITH_CYCLES_STANDALONE_GUI:BOOL=FALSE
        -DWITH_DOC_MANPAGE:BOOL=TRUE
        -DWITH_DRACO:BOOL=FALSE
        -DWITH_FREESTYLE:BOOL=TRUE
        -DWITH_GHOST_DEBUG:BOOL=FALSE
        -DWITH_GHOST_SDL:BOOL=FALSE
        -DWITH_GHOST_WAYLAND_DYNLOAD:BOOL=FALSE
        -DWITH_GHOST_X11:BOOL=TRUE
        -DWITH_GHOST_XDND:BOOL=TRUE
        -DWITH_GMP:BOOL=TRUE
        -DWITH_GPU_BUILDTIME_SHADER_BUILDER:BOOL=FALSE
        -DWITH_GTESTS:BOOL=FALSE
        -DWITH_HARU:BOOL=FALSE
        -DWITH_HEADLESS:BOOL=FALSE
        -DWITH_HYDRA:BOOL=FALSE
        -DWITH_IK_ITASC:BOOL=TRUE
        -DWITH_IK_SOLVER:BOOL=TRUE
        -DWITH_IMAGE_CINEON:BOOL=TRUE
        -DWITH_IMAGE_OPENEXR:BOOL=TRUE
        -DWITH_INPUT_NDOF:BOOL=FALSE
        -DWITH_INSTALL_COPYRIGHT:BOOL=FALSE
        -DWITH_INSTALL_PORTABLE:BOOL=FALSE
        -DWITH_INTERNATIONAL:BOOL=TRUE
        -DWITH_IO_GPENCIL:BOOL=FALSE
        -DWITH_IO_PLY:BOOL=TRUE
        -DWITH_IO_STL:BOOL=TRUE
        -DWITH_IO_WAVEFRONT_OBJ:BOOL=TRUE
        -DWITH_LIBMV:BOOL=TRUE
        -DWITH_LLVM:BOOL=FALSE
        -DWITH_LZMA:BOOL=TRUE
        -DWITH_LZO:BOOL=TRUE
        -DWITH_MATERIALX:BOOL=FALSE
        -DWITH_MEM_JEMALLOC:BOOL=FALSE
        -DWITH_MEM_VALGRIND:BOOL=FALSE
        -DWITH_METAL_BACKEND:BOOL=FALSE
        -DWITH_MOD_FLUID:BOOL=TRUE
        -DWITH_MOD_REMESH:BOOL=TRUE
        -DWITH_NANOVDB:BOOL=FALSE
        -DWITH_OPENCOLLADA:BOOL=FALSE
        -DWITH_OPENCOLORIO:BOOL=TRUE
        -DWITH_OPENGL_BACKEND:BOOL=TRUE
        -DWITH_OPENGL_DRAW_TESTS:BOOL=FALSE
        -DWITH_OPENGL_RENDER_TESTS:BOOL=FALSE
        -DWITH_OPENIMAGEDENOISE:BOOL=FALSE
        -DWITH_OPENSUBDIV:BOOL=TRUE
        -DWITH_OPENVDB:BOOL=FALSE
        -DWITH_OPENVDB_3_ABI_COMPATIBLE:BOOL=FALSE
        -DWITH_OPENVDB_BLOSC:BOOL=FALSE
        -DWITH_POTRACE:BOOL=FALSE
        -DWITH_PUGIXML:BOOL=TRUE
        -DWITH_PULSEAUDIO:BOOL=FALSE
        -DWITH_PYTHON_INSTALL:BOOL=FALSE
        -DWITH_PYTHON_INSTALL_NUMPY:BOOL=FALSE
        -DWITH_PYTHON_INSTALL_REQUESTS:BOOL=FALSE
        -DWITH_PYTHON_MODULE:BOOL=FALSE
        -DWITH_PYTHON_NUMPY:BOOL=FALSE
        -DWITH_QUADRIFLOW:BOOL=TRUE
        -DWITH_RENDERDOC:BOOL=FALSE
        -DWITH_SDL_DYNLOAD:BOOL=FALSE
        -DWITH_STATIC_LIBS:BOOL=FALSE
        -DWITH_STRICT_BUILD_OPTIONS:BOOL=TRUE
        -DWITH_STRSIZE_DEBUG:BOOL=FALSE
        -DWITH_SYSTEM_AUDASPACE:BOOL=FALSE
        -DWITH_SYSTEM_BULLET:BOOL=FALSE
        -DWITH_SYSTEM_EIGEN3:BOOL=TRUE
        -DWITH_SYSTEM_FREETYPE:BOOL=TRUE
        -DWITH_SYSTEM_LZO:BOOL=TRUE
        -DWITH_TBB:BOOL=TRUE
        -DWITH_UNITY_BUILD:BOOL=TRUE
        -DWITH_USD:BOOL=FALSE
        -DWITH_VULKAN_BACKEND:BOOL=FALSE
        -DWITH_VULKAN_GUARDEDALLOC:BOOL=FALSE
        -DWITH_X11_XF86VMODE:BOOL=TRUE
        -DWITH_X11_XFIXES:BOOL=TRUE
        -DWITH_X11_XINPUT:BOOL=TRUE
        -DWITH_XR_OPENXR:BOOL=FALSE
        $(cmake_with ffmpeg CODEC_FFMPEG)
        $(cmake_with fftw FFTW3)
        $(cmake_with fftw MOD_OCEANSIM)
        $(cmake_with jack JACK)
        $(cmake_with jpeg2000 IMAGE_OPENJPEG)
        $(cmake_with openal OPENAL)
        $(cmake_with openmp OPENMP)
        $(cmake_with sdl SDL)
        $(cmake_with sndfile CODEC_SNDFILE)
        $(cmake_with wayland GHOST_WAYLAND)
        $(cmake_with wayland GHOST_WAYLAND_DBUS)
        $(cmake_with wayland GHOST_WAYLAND_LIBDECOR)
        $(cmake_with webp IMAGE_WEBP)
    )
    # Blender doesn't use pkg-config to find ffmpeg, and its normal search routines find the
    # currently selected system ffmpeg alternative instead of the desired ffmpeg. Prepend the
    # alternatives directory to the CMAKE_FIND_ROOT_PATH.
    if optionq ffmpeg ; then
        args+=(
            -DCMAKE_FIND_ROOT_PATH:PATH="$(ffmpeg_alternatives_prefix);/usr/$(exhost --target)"
        )
    fi

    ecmake "${args[@]}"

    # Fixes the infamous implicit + "-isystem /usr/include " insertion bug by CMake for some components provided by blender.
    # A long investigation led me to think that we might have some sort of CMake issue, actually:
    #  1. https://developers.redhat.com/blog/2016/02/29/why-cstdlib-is-more-complicated-than-you-might-think/
    #  2. https://stackoverflow.com/questions/37218953/isystem-on-a-system-include-directory-causes-errors
    #  3. https://gitlab.kitware.com/cmake/cmake/issues/19095
    #  4. https://gitlab.kitware.com/cmake/cmake/merge_requests/3157 (as Exherbo upgraded to 3.14.5, this MR should be already merged)
    # CMake tries to parse implicit include info, that comes from the actual toolchain:
    # /usr/share/cmake/Modules/CMakeParseImplicitIncludeInfo.cmake
    # But it doesn't detect anything for a GNU CC toolchain, so it is inserting " -isystem /usr/include " in some invocations of a CPP compiler.
    # It happens a lot at Exherbo because it always cross-compile.
    # I asked CMake upstream why this parser won't check for GCC information (4).
    edo find -name flags.make -exec sed -i -e 's: -isystem /usr/include : :g' {} \;
}

blender_src_install() {
    cmake_src_install

    # TODO: blender fails to run otherwise
    dodir /usr/$(exhost --target)/share
    dosym /usr/share/${PN} /usr/$(exhost --target)/share/${PN}

    edo mv "${IMAGE}"/usr/share/doc/{${PN},${PNVR}}

    keepdir /usr/share/${PN}/${MY_PV}/scripts/addons_contrib
}

