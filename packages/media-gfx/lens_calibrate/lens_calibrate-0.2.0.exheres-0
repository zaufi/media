# Copyright 2019-2020 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ user=cryptomilk tag=${PNV} suffix=tar.bz2 ] \
    python [ blacklist=2 multibuild=false ]

SUMMARY="Script to calculate lens calibration data for the lensfun project"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: graphicsmagick imagemagick ) [[
        number-selected = at-most-one
    ]]
"

DEPENDENCIES="
    run:
        dev-python/numpy[python_abis:*(-)?]
        dev-python/py3exiv2[>=0.2.1][python_abis:*(-)?]
        dev-python/PyPDF2[python_abis:*(-)?]
        dev-python/scipy[python_abis:*(-)?]
        media-gfx/darktable[>=3.0.0]
        media-gfx/hugin[>=2018.0.0]
        sci-apps/gnuplot
        providers:graphicsmagick? ( media-gfx/GraphicsMagick[imagemagick][tiff] )
        providers:imagemagick? ( media-gfx/ImageMagick[tiff][raw] )
"

src_prepare() {
    # fix shebang and help output
    edo sed \
        -e "s:#!/usr/bin/python3:#!/usr/bin/env python$(python_get_abi):g" \
        -e "s:${PN}.py:${PN}:g" \
        -i ${PN}.py

    default
}

src_install() {
    default

    newbin ${PN}.py ${PN}
}

