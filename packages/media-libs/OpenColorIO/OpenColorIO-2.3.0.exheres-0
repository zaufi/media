# Copyright 2019 Rune Morling <ermo.exherbo.org@spammesenseless.net>
# Distributed under the terms of the GNU General Public license v2

require github [ user=AcademySoftwareFoundation tag=v${PV} ] cmake

SUMMARY="A color management framework for visual effects and animation"

HOMEPAGE+=" https://opencolorio.org"

LICENCES="
    BSD-3
    ICC [[ note = [ bundled SampleICC ] ]]
    ZLIB [[ note = [ bundled MD5 ] ]]
"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# test_cpu segfaults, last checked: 2.2.1
RESTRICT="test"

DEPENDENCIES="
    build+run:
        app-arch/minizip-ng[>=3.0.6][-zstd]
        dev-cpp/pystring[>=1.1.3]
        dev-libs/expat[>=2.4.1]
        dev-libs/yaml-cpp:=[>=0.6.3]
        media-libs/imath[>=3.1.1]
        sys-libs/zlib[>=1.2.8]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DOCIO_BUILD_APPS:BOOL=FALSE
    -DOCIO_BUILD_DOCS:BOOL=FALSE
    -DOCIO_BUILD_FROZEN_DOCS:BOOL=FALSE
    -DOCIO_BUILD_GPU_TESTS:BOOL=FALSE
    -DOCIO_BUILD_JAVA:BOOL=FALSE
    -DOCIO_BUILD_NUKE:BOOL=FALSE
    -DOCIO_BUILD_OPENFX:BOOL=FALSE
    -DOCIO_BUILD_PYTHON:BOOL=FALSE
    -DOCIO_ENABLE_SANITIZER:BOOL=FALSE
    -DOCIO_USE_HEADLESS:BOOL=FALSE
    -DOCIO_USE_OIIO_FOR_APPS:BOOL=FALSE
    -DOCIO_USE_SIMD:BOOL=TRUE
    -DOCIO_USE_SOVERSION:BOOL=TRUE
    -DOCIO_VERBOSE:BOOL=TRUE
    -DOCIO_WARNING_AS_ERROR:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DOCIO_BUILD_TESTS:BOOL=TRUE -DOCIO_BUILD_TESTS:BOOL=FALSE'
)

