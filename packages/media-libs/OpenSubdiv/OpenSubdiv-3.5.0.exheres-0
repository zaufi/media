# Copyright 2019 Rune Morling <ermo.exherbo.org@spammesenseless.net>
# Distributed under the terms of the GNU General Public license v2

require github [ user=PixarAnimationStudios tag=v${PV//./_} ] cmake

SUMMARY="An Open-Source subdivision surface library"

HOMEPAGE+=" https://graphics.pixar.com/opensubdiv"

LICENCES="Apache-2.0"
SLOT=0
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3]
    build+run:
        dev-libs/libglvnd
        sys-libs/libgomp:=
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DNO_CLEW:BOOL=TRUE
    -DNO_CUDA:BOOL=TRUE
    -DNO_DOC:BOOL=TRUE
    -DNO_DX:BOOL=TRUE
    -DNO_EXAMPLES:BOOL=TRUE
    -DNO_GLEW:BOOL=TRUE
    -DNO_GLFW:BOOL=TRUE
    -DNO_GLFW_X11:BOOL=TRUE
    -DNO_LIB:BOOL=FALSE
    -DNO_MACOS_FRAMEWORK:BOOL=TRUE
    -DNO_METAL:BOOL=TRUE
    -DNO_OMP:BOOL=FALSE
    -DNO_OPENCL:BOOL=TRUE
    -DNO_OPENGL:BOOL=FALSE
    -DNO_PTEX:BOOL=TRUE
    -DNO_TBB:BOOL=TRUE
    -DNO_TUTORIALS:BOOL=TRUE
    -DOPENSUBDIV_GREGORY_EVAL_TRUE_DERIVATIVES:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DNO_GLTESTS:BOOL=FALSE -DNO_GLTESTS:BOOL=TRUE'
    '-DNO_REGRESSION:BOOL=FALSE -DNO_REGRESSION:BOOL=TRUE'
    '-DNO_TESTS:BOOL=FALSE -DNO_TESTS:BOOL=TRUE'
)

