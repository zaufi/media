# Copyright 2009 Mike Kelly <pioto@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=xiph ] \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 ] ]

SUMMARY="A cross-platform audio library"
DESCRIPTION="
Libao is a cross-platform audio library that allows programs to output
audio using a simple API on a wide variety of platforms. It currently
supports:

  * Null output (handy for testing without a sound device)
  * WAV files
  * AU files
  * OSS (Open Sound System, used on Linux and FreeBSD)
  * ALSA (Advanced Linux Sound Architecture)
  * PulseAudio (next generation GNOME sound server)
  * esd (EsounD or Enlightened Sound Daemon)
  * AIX
  * Sun/NetBSD/OpenBSD
  * IRIX
  * NAS (Network Audio Server)
"
HOMEPAGE+=" https://www.xiph.org/ao"

REMOTE_IDS="freecode:${PN}"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    alsa
    mmap [[ requires = alsa
            description = [ Use mmio with ALSA, another method of how ALSA accesses devices ] ]]
    pulseaudio
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        alsa? ( sys-sound/alsa-lib[>=0.9] )
        pulseaudio? ( media-sound/pulseaudio[>=0.9] )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-pulse-fix-missing-include-warning-for-nanosleep.patch
)

# disable things we don't have support for
DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-{arts,esd{,test},nas,wmm}
    --without-x
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    alsa
    'alsa broken-oss'
    'mmap alsa-mmap'
    'pulseaudio pulse'
)

src_prepare() {
    # TODO(sardemff7) Patch upstream
    # Fix headers installation path
    edo sed -i \
        -e '/^includedir/a\aoincludedir = $(includedir)/ao' \
        -e '/^includedir/d' \
        -e '/^include_HEADERS/s:^:ao:' \
        include/ao/Makefile.am

    autotools_src_prepare
}

