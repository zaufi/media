# Copyright 2009 Thomas Anderson <tanderson@caltech.edu>
# Distributed under the terms of the GNU General Public License v2

require github [ release=${PV} suffix=tar.xz ]

SUMMARY="ASS/SSA (Advanced Substation Alpha/Substation Alpha) subtitle rendering library"
DESCRIPTION="
The ASS format can be used for extremely complex subtitles. Text can be rendered with
borders and shadows in arbitrary colors, with alpha transparency and arbitrarily positioned.
Additionally, various vectors transformations (rotations, shearing, scaling) and bitmap
operations (box blur, gaussian blur) can be applied. Finally, it is also possible to clip
resulting subtitles. There are even more advanced features available, like vector drawings,
vector blending and animations.
"

LICENCES="ISC"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    fontconfig
    unicode [[
        description = [ Line breaks in accordance with the Unicode line breaking algorithm ]
    ]]
    platform: amd64 x86
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
        platform:amd64? ( dev-lang/nasm[>=2.10] )
        platform:x86? ( dev-lang/nasm[>=2.10] )
    build+run:
        dev-libs/fribidi[>=0.19.1]
        media-libs/freetype:2[>=2.3.6]
        x11-libs/harfbuzz[>=1.2.3]
        fontconfig? ( media-libs/fontconfig[>=2.10.92] )
        unicode? ( dev-libs/libunibreak )
    test:
        media-libs/libpng:=[>=1.2.0]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-coretext
    --disable-directwrite
    --disable-fuzz
    --disable-static
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    fontconfig
    'fontconfig require-system-font-provider'
    'platform:amd64 asm'
    'platform:x86 asm'
    'unicode libunibreak'
)
DEFAULT_SRC_CONFIGURE_TESTS=(
    '--enable-compare --disable-compare'
    '--enable-test --disable-test'
    ART_SAMPLES=${PN}-tests-compatible_${PV}
)

src_fetch_extra() {
    if expecting_tests && [[ ! -e "${FETCHEDDIR}"/compatible_${PV}.tar.gz ]] ; then
        edo wget -P "${FETCHEDDIR}" https://github.com/libass/libass-tests/archive/refs/tags/compatible_${PV}.tar.gz
    fi
}

src_unpack() {
    default

    edo pushd ${PNV}
    expecting_tests && unpack --if-compressed compatible_${PV}.tar.gz
    edo popd
}

src_test() {
    LD_LIBRARY_PATH="${WORK}" emake test
}

