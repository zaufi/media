# Copyright 2010 Xavier Barrachina <xabarci@doctor.upv.es>
# Distributed under the terms of the GNU General Public License v2

require github [ user=ianw release=release_${PV//./_} suffix=tar.gz ]

SUMMARY="Library for manipulating IPTC metadata"
DESCRIPTION="
libiptcdata is a library, written in C, for manipulating the International Press Telecommunications
Council (IPTC) metadata stored within multimedia files such as images. This metadata can include
captions and keywords, often used by popular photo management applications. The library provides
routines for parsing, viewing, modifying, and saving this metadata.
"

LICENCES="LGPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    gtk-doc
    ( linguas: de it )
"

# gtk-doc test fails, last checked: 1.0.5
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
        gtk-doc? ( dev-doc/gtk-doc[>=1.14] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --disable-python
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    gtk-doc
)

src_install() {
    default

    # do not install pre-built gtk-doc documentation
    ! option gtk-doc && edo rm -rf "${IMAGE}"/usr/share/gtk-doc
}

