# Copyright 2019-2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=georgmartius project=vid.stab tag=v${PV} ] \
    cmake \
    toolchain-funcs

SUMMARY="Library for stabilizing video clips"
HOMEPAGE+=" http://public.hronopik.de/vid.stab"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="openmp"

DEPENDENCIES="
    build+run:
        openmp? ( sys-libs/libgomp:= )
"

src_prepare() {
    cmake_src_prepare

    # remove automagic check for SSE2
    edo sed \
        -e 's:include (FindSSE)::g' \
        -i CMakeLists.txt
}

src_configure() {
    local cmakeparams=(
        -DBUILD_SHARED_LIBS:BOOL=TRUE
        $(cmake_use openmp OMP)
        $(cc-has-defined __SSE2__ && echo -DSSE2_FOUND:BOOL=TRUE || echo -DSSE2_FOUND:BOOL=FALSE)
    )

    ecmake "${cmakeparams[@]}"
}

