# Copyright 2008 Anders Ossowicki <arkanoid@exherbo.org>
# Copyright 2014 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam tag

require gitlab [ prefix='https://code.videolan.org' user='videolan' tag=$(exparam tag) suffix=tar.bz2 new_download_scheme=true ]
require bash-completion
require alternatives

export_exlib_phases src_configure src_test src_install

SUMMARY="Library for encoding H264/AVC video streams"
HOMEPAGE+=" https://www.videolan.org/developers/${PN}.html"

LICENCES="GPL-2 GPL-3"
MYOPTIONS="
    visualisation [[ description = [ Visualize the encoding process ] ]]
    platform: amd64 x86
"

DEPENDENCIES="
    build:
        platform:amd64? ( dev-lang/nasm[>=2.13] )
        platform:x86? ( dev-lang/nasm[>=2.13] )
    build+run:
        visualisation? ( x11-libs/libX11 )
    run:
        !media-libs/x264:0[<20210211-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

_x264_assembler() {
    case "$(exhost --target)" in
    i686-*|x86_64-*)
        echo nasm
    ;;
    *)
        echo $(exhost --tool-prefix)cc
    ;;
    esac
}

x264_src_configure() {
    export AS=$(_x264_assembler)

    local myconf=(
        # prefixed string executable isn\'t found otherwise
        --cross-prefix=$(exhost --tool-prefix)
        --enable-shared
        --disable-avs
        --disable-bashcompletion
        --disable-ffms
        --disable-gpac
        --disable-lavf
        --disable-swscale
    )

    # --disable-visualize doesn't exist so don't use option_enable
    option visualisation && myconf+=( "--enable-visualize" )

    econf "${myconf[@]}"
}

x264_src_test() {
    :
}

x264_src_install() {
    local arch_dependent_alternatives=()
    local host=$(exhost --target)

    default

    dobashcompletion tools/bash-autocomplete.sh x264

    arch_dependent_alternatives=(
        /usr/${host}/bin/${PN}              ${PN}-${SLOT}
        /usr/${host}/include/${PN}_config.h ${PN}_config-${SLOT}.h
        /usr/${host}/include/${PN}.h        ${PN}-${SLOT}.h
        /usr/${host}/lib/lib${PN}.so        lib${PN}-${SLOT}.so
        /usr/${host}/lib/pkgconfig/${PN}.pc ${PN}-${SLOT}.pc
    )


    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"

    if option bash-completion ; then
        alternatives_for _${PN} ${SLOT} ${SLOT} \
            /usr/share/bash-completion/completions/${PN} ${PN}-${SLOT}
    fi
}

