# Copyright 2008 Richard Brown
# Copyright 2009 Bo Ørsted Andresen
# Copyright 2009 Thomas Anderson
# Copyright 2011 Ali Polatel
# Copyright 2011-2012 Quentin "Sardem FF7" Glidic <sardemff7@exherbo.org>
# Copyright 2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require meson [ meson_minimum_version=0.56.0 ] systemd-service
require ffmpeg [ with_opt=true ]
require toolchain-funcs

export_exlib_phases pkg_pretend pkg_setup src_configure src_install

SUMMARY="A flexible, powerful, server-side application for playing music"
DESCRIPTION="
Music Player Daemon (MPD) is a flexible, powerful, server-side application for playing music.
Through plugins and libraries it can play a variety of sound files while being controlled by
its network protocol.
"
HOMEPAGE="https://www.musicpd.org"
if ! ever is_scm; then
        DOWNLOADS="${HOMEPAGE}/download/${PN}/$(ever range -2)/${PNV}.tar.xz"
fi

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    aac
    alsa
    avahi
    cdio [[ description = [ CD support through libcdio ] ]]
    chromaprint [[ description = [ Support for finding AcoustID fingerprints of audio files ] ]]
    curl [[ description = [ Support obtaining song data via HTTP and enable the WebDAV storage plugin ] ]]
    dbus
    doc
    dsd [[ description = [ Support for decoding DSD ] ]]
    id3 [[ description = [ Support for ID3 tags ] ]]
    io_uring [[ description = [ Plugin for playing local files on Linux using liburing ] ]]
    jack [[ description = [ Enable jack-audio-connection-kit audio output ] ]]
    libmpdclient [[ description = [ Enable support for remote mpd databases ] ]]
    libsamplerate
    mms [[ description = [ Microsoft Media Server protocol support ] ]]
    nfs [[ description = [ Enable support for streaming music over a NFS share ] ]]
    ogg
    openal
    opus [[ description = [ Opus codec support ] requires = ogg ]]
    pipewire [[ description = [ Adds support for the PipeWire sound server ] ]]
    pulseaudio
    qobuz [[
        description = [ Qobuz music streaming support (input) ]
        requires = [ curl yaml ]
    ]]
    samba [[ description = [ Enable support for streaming music over a SMB share ] ]]
    snapcast [[
        description = [ Add an Snapcast output plugin ]
        requires = [ yaml ]
    ]]
    shout [[ description = [ Enable support for streaming through shout (mp3, and ogg if ogg is enabled) ] ]]
    sndfile
    sndio [[ description = [ Adds support for sound output through sndio
                             (OpenBSD sound API, also ported to Linux) ] ]]
    soundcloud [[
        description = [ SoundCloud.com support (input) ]
        requires = [ curl yaml ]
    ]]
    soxr [[ description = [ Enable support for the libsoxr resampler ] ]]
    sqlite [[ description = [ Enable support for storing the MPD database in an Sqlite database ] ]]
    systemd [[ description = [ systemd socket activation support ] ]]
    udisks [[ description = [ Support for removable media using udisks2 ] ]]
    upnp [[
        description = [ Support Service Discovery via UPnP ]
        requires = curl
    ]]
    yaml [[ description = [ YAML support via libyajl ] ]]
    zip [[ description = [ zip archive support ] ]]
    (
        aac
        audiofile [[ description = [ Enable audiofile support, enables wave support ] ]]
        ffmpeg [[ description = [ Enable the ffmpeg input plugin, allowing you to play all audio formats supported by ffmpeg ] ]]
        mikmod
        modplug [[ description = [ mod-like file format support ] ]]
        mp3
        musepack
        wavpack
        ( flac shout vorbis ) [[ requires = ogg ]]
    ) [[ number-selected = at-least-one ]]
    upnp? (
        ( providers: libnpupnp libupnp ) [[
            number-selected = exactly-one
            *requires = upnp
        ]]
    )
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        dev-python/Sphinx
    build+run:
        app-arch/bzip2
        dev-libs/expat
        dev-libs/fmt
        dev-libs/icu:=[>=50]
        dev-libs/pcre2
        aac? ( media-libs/faad2 )
        alsa? ( sys-sound/alsa-lib[>=0.9.0] )
        audiofile? ( media-libs/audiofile[>=0.3] )
        avahi? ( net-dns/avahi )
        cdio? (
            dev-libs/libcdio
            dev-libs/libcdio-paranoia[>=0.93_p1]
        )
        chromaprint? ( media-libs/chromaprint )
        curl? ( net-misc/curl[>=7.55] )
        dbus? ( sys-apps/dbus )
        flac? ( media-libs/flac:=[>=1.2][ogg?] )
        id3? ( media-libs/libid3tag )
        io_uring? ( sys-libs/liburing )
        jack? ( media-sound/jack-audio-connection-kit[>=0.100] )
        libmpdclient? ( media-libs/libmpdclient[>=2.11] )
        libsamplerate? ( media-libs/libsamplerate[>=0.1.3] )
        mms? ( media-libs/libmms[>=0.4] )
        mikmod? ( media-libs/libmikmod[>=3.2] )
        modplug? ( media-libs/libmodplug )
        mp3? (
            media-libs/libmad
            media-sound/lame
        )
        musepack? ( media-libs/musepack )
        nfs? ( net-fs/libnfs[>=1.11] )
        ogg? ( media-libs/libogg )
        openal? ( media-libs/openal )
        opus? ( media-libs/opus )
        pipewire? ( media/pipewire[>=0.3] )
        pulseaudio? ( media-sound/pulseaudio[>=0.9.16] )
        qobuz? ( dev-libs/libgcrypt )
        samba? ( net-fs/samba )
        shout? ( media-libs/libshout[>=2.4.0] )
        snapcast? ( net-apps/snapcast )
        sndfile? ( media-libs/libsndfile )
        sndio? ( sys-sound/sndio )
        soxr? ( media-libs/soxr )
        sqlite? ( dev-db/sqlite:3[>=3.7.3] )
        systemd? ( sys-apps/systemd )
        providers:libnpupnp? (
            net-libs/libnpupnp[>=1.8]
        )
        providers:libupnp? (
            net-libs/libupnp[>=1.8]
        )
        vorbis? ( media-libs/libvorbis )
        wavpack? ( media-sound/wavpack[>=5] )
        yaml? ( dev-libs/yajl[>=2.0] )
        zip? ( dev-libs/zziplib[>=0.13] )
    test:
        dev-cpp/gtest
    suggestion:
        media-sound/ario [[ description = [ Provides rhythmbox-like client ] ]]
        media-sound/gmpc [[ description = [ Provides fast and fully featured GTK-based client ] ]]
        media-sound/mpc [[ description = [ Provides command line client ] ]]
        media-sound/mpdcron [[ description = [ Executes scripts based on mpd's idle events ] ]]
        media-sound/ncmpc [[ description = [ Provides ncurses based command line client ] ]]
        media-sound/pms [[ description = [ Provides an alternative ncurses based command line client ] ]]
        media-sound/qmpdclient [[ description = [ Provides simple QT client ] ]]
        media-sound/sonata [[ description = [ Provides an elegant GTK-based client ] ]]
        sys-sound/oss [[ description = [ Provides an alternative sound architecture instead of ALSA ] ]]
"

mpd_pkg_pretend() {
    if cc-is-gcc && ! ever at_least 8 $(gcc-version) ; then
        eerror "sys-devel/gcc[>=8] is required to build media-sound/mpd."
        eerror "You can use 'eclectic gcc' to change the active compiler."
        die
    fi
}

mpd_pkg_setup() {
    meson_pkg_setup
    ffmpeg_pkg_setup
}

mpd_src_configure() {
    local meson_params=(
        -Dadplug=disabled
        -Dao=disabled
        -Dbzip2=enabled
        -Dcue=true
        -Ddaemon=true
        -Ddatabase=true
        -Ddocumentation=enabled
        -Depoll=true
        -Deventfd=true
        -Dexpat=enabled
        -Dfifo=true
        -Dfluidsynth=disabled
        -Dfuzzer=false
        -Dgme=disabled
        -Dhttpd=true
        -Diconv=disabled
        -Dicu=enabled
        -Dinotify=true
        -Dipv6=enabled
        -Diso9660=disabled
        -Dlocal_socket=true
        -Dmanpages=true
        -Dmpg123=disabled
        -Dneighbor=true
        -Dopenmpt=disabled
        -Doss=enabled
        -Dpcre=enabled
        -Dpipe=true
        -Drecorder=true
        -Dshine=disabled
        -Dsidplay=disabled
        -Dsignalfd=true
        -Dsolaris_output=disabled
        -Dsyslog=enabled
        -Dtcp=true
        # libvorbis is preferred anyway, could be an option for embedded devices
        -Dtremor=disabled
        -Dtwolame=disabled
        -Dwave_encoder=true
        -Dwildmidi=disabled
        -Dzlib=enabled

        $(meson_switch avahi zeroconf avahi disabled)
        $(meson_switch doc html_manual)
        $(meson_switch dsd)
        $(meson_switch snapcast)
        $(meson_switch systemd systemd_system_unit_dir "${SYSTEMDSYSTEMUNITDIR}")
        $(meson_switch systemd systemd_user_unit_dir "${SYSTEMDUSERUNITDIR}")

        $(meson_feature aac faad)
        $(meson_feature alsa)
        $(meson_feature audiofile)
        $(meson_feature cdio cdio_paranoia)
        $(meson_feature chromaprint)
        $(meson_feature curl)
        $(meson_feature curl webdav)
        $(meson_feature dbus)
        $(meson_feature ffmpeg)
        $(meson_feature flac)
        $(meson_feature id3 id3tag)
        $(meson_feature io_uring)
        $(meson_feature jack)
        $(meson_feature libmpdclient)
        $(meson_feature libsamplerate)
        $(meson_feature mms)
        $(meson_feature mikmod)
        $(meson_feature modplug)
        $(meson_feature mp3 mad)
        $(meson_feature mp3 lame)
        $(meson_feature musepack mpcdec)
        $(meson_feature nfs)
        $(meson_feature openal)
        $(meson_feature opus)
        $(meson_feature pipewire)
        $(meson_feature pulseaudio pulse)
        $(meson_feature qobuz)
        $(meson_feature shout)
        $(meson_feature sndfile)
        $(meson_feature sndio)
        $(meson_feature samba smbclient)
        $(meson_feature soundcloud)
        $(meson_feature soxr)
        $(meson_feature sqlite)
        $(meson_feature systemd)
        $(meson_feature udisks)
        $(meson_feature vorbis)
        $(meson_feature vorbis vorbisenc)
        $(meson_feature wavpack)
        $(meson_feature yaml yajl)
        $(meson_feature zip zzip)

        $(expecting_tests -Dtest=true -Dtest=false)
    )

    if option upnp ; then
        if option providers:libupnp ; then
            meson_params+=( -Dupnp=pupnp )
        else
            meson_params+=( -Dupnp=npupnp )
        fi
    else
        meson_params+=( -Dupnp=disabled )
    fi

    exmeson "${meson_params[@]}"
}

mpd_src_install() {
    meson_src_install

    # Don't install docs a second time into an undesired dir, emagicdocs
    # already installs them.
    edo rm "${IMAGE}"/usr/share/doc/${PN}/{AUTHORS,COPYING,NEWS,README.md}
    if option doc; then
        # https://github.com/MusicPlayerDaemon/MPD/issues/971
        edo mv "${IMAGE}"/usr/share/doc/${PN}/html "${IMAGE}"/usr/share/doc/${PNVR}
    fi
    edo rmdir "${IMAGE}"/usr/share/doc/${PN}
}

