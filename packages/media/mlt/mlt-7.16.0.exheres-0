# Copyright 2009-2010 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=mltframework release=v${PV} suffix=tar.gz ]
require cmake
require ffmpeg [ with_opt=true ]
require python [ blacklist=none multibuild=false with_opt=true ]
require alternatives
require flag-o-matic

SUMMARY="Multimedia authoring and processing framework and a video playout server for television broadcasting"
DESCRIPTION="
MLT is a multimedia framework designed for television broadcasting. As such, it
provides a pluggable architecture for the inclusion of new audio/video sources,
filters, transitions and playback devices.
"
HOMEPAGE+=" https://www.mltframework.org"

UPSTREAM_RELEASE_NOTES="https://www.mltframework.org/blog/v${PV}_released/"

LICENCES="LGPL-2.1 GPL-2 GPL-3"
SLOT="7"
PLATFORMS="~amd64 ~x86"
# Some don't have an install target and all set the their name to mlt, which
# makes it hard to install them manually.
# lua, mono, php, ruby, tcl
SWIG_LANGUAGES="java nodejs perl python"
MYOPTIONS="doc ffmpeg gtk jack libsamplerate opengl ogg qt5 sdl sox
    ${SWIG_LANGUAGES}
    frei0r [[ description = [ Adds support for frei0r video effects plugins ] ]]
    glaxnimate [[ description = [ Adds support for Glaxnimate 2D animations ] ]]
    nodejs [[ description = [ Adds SWIG Node.js bindings ] ]]
    plus   [[ description = [ Adds support for additional filters ] ]]
    qt6    [[ description = [ Adds support for the Qt GUI/Application Toolkit version 6.x ] ]]
    rubberband [[ description = [ Add support for audio pitch-shifting ] ]]
    vidstab [[ description = [ Add support for performing video stabilization/deshaking ] ]]

    ( glaxnimate [[ requires = qt5 ]] )
    ( platform: amd64 x86 )
    ( x86_cpu_features: mmx sse sse2 )
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? ( app-doc/doxygen )
    build+run:
        dev-libs/libxml2:2.0[>=2.5]
        sys-sound/alsa-lib
        frei0r? ( media-plugins/frei0r-plugins )
        glaxnimate? (
            app-arch/libarchive
            sys-libs/zlib
        )
        gtk? (
            media-libs/fontconfig
            media-libs/libexif
            x11-libs/gdk-pixbuf:2.0
            x11-libs/pango
        )
        jack? (
            dev-libs/glib:2
            media-libs/ladspa-sdk
            media-sound/jack-audio-connection-kit
        )
        java? (
            dev-lang/swig
            virtual/jdk:=
        )
        libsamplerate? ( media-libs/libsamplerate[>=0.1.5] )
        nodejs? (
            dev-lang/node[>=12]
            dev-lang/swig[>=4.1]
        )
        ogg? ( media-libs/libvorbis )
        opengl? (
            media-libs/movit
            x11-dri/mesa
            x11-libs/libX11
        )
        perl? (
            dev-lang/perl:=[>=5&<6]
            dev-lang/swig
        )
        plus? (
            media-libs/libebur128
            sci-libs/fftw[>=3]
        )
        python? (
            dev-lang/python:=[>=3]
            dev-lang/swig
        )
        qt5? (
            media-libs/libexif
            sci-libs/fftw[>=3]
            x11-libs/libX11
            x11-libs/qtbase:5
            x11-libs/qtsvg:5
        )
        qt6? (
            x11-libs/qtbase:6
            x11-libs/qt5compat:6
            x11-libs/qtsvg:6
        )
        rubberband? ( media-libs/rubberband )
        sdl? (
            media-libs/SDL:2[X]
                x11-libs/libX11
        )
        sox? ( media-sound/sox )
        vidstab? ( media-libs/libvidstab )
"

pkg_setup() {
    ffmpeg_pkg_setup

    if [[ $(exhost --target) == *-musl* ]] ; then
        append-flags "-DHAVE_LOCALE_H=1"
    fi
}

src_configure() {
    local cmake_params=(
        -DCLANG_FORMAT:BOOL=OFF
        -DGPL:BOOL=ON
        -DGPL3:BOOL=ON
        -DRELOCATABLE:BOOL=OFF

        -DMOD_KDENLIVE:BOOL=ON
        -DMOD_NORMALIZE:BOOL=ON
        -DMOD_OLDFILM:BOOL=ON
        -DMOD_XML:BOOL=ON

        -DMOD_DECKLINK:BOOL=OFF
        -DMOD_GLAXNIMATE_QT6:BOOL=OFF
        -DMOD_NDI:BOOL=OFF
        -DMOD_OPENCV:BOOL=OFF
        # Microsoft produced adaptive wide-band speech codec, needs PulseAudio
        -DMOD_RTAUDIO:BOOL=OFF
        -DMOD_SDL1:BOOL=OFF
        -DMOD_XINE:BOOL=OFF

        # See comment above MYOPTIONS about general problems with bindings
        # cmake/Find{Mono,PHP}.cmake appear slightly broken
        -DSWIG_CSHARP:BOOL=OFF
        -DSWIG_LUA:BOOL=OFF
        -DSWIG_PHP:BOOL=OFF
        # Fails to build: error: call of overloaded
        # 'rb_define_virtual_variable(const char [21], VALUE (&)(...), NULL)' is ambiguous
        -DSWIG_RUBY:BOOL=OFF
        -DSWIG_TCL:BOOL=OFF

        $(cmake_build doc DOCS)
        $(cmake_option ffmpeg MOD_AVFORMAT)
        $(cmake_option frei0r MOD_FREI0R)
        $(cmake_option glaxnimate MOD_GLAXNIMATE)
        $(cmake_option gtk MOD_GDK)
        $(cmake_option jack MOD_JACKRACK)
        $(cmake_option libsamplerate MOD_RESAMPLE)
        $(cmake_option ogg MOD_VORBIS)
        $(cmake_option opengl MOD_MOVIT)
        $(cmake_option plus MOD_PLUS)
        $(cmake_option plus MOD_PLUSGPL)
        $(cmake_option qt5 MOD_QT)
        $(cmake_option qt6 MOD_QT6)
        $(cmake_option qt6 BUILD_TESTS_WITH_QT6)
        $(cmake_option rubberband MOD_RUBBERBAND)
        $(cmake_option sdl MOD_SDL2)
        $(cmake_option sox MOD_SOX)
        $(cmake_option vidstab MOD_VIDSTAB)

        $(cmake_option java SWIG_JAVA)
        $(cmake_option nodejs SWIG_NODEJS)
        $(cmake_option perl SWIG_PERL)
        $(cmake_option python SWIG_PYTHON)
    )

    ecmake "${cmake_params[@]}"
}

src_compile() {
    default

    option doc && emake docs
}

src_install() {
    cmake_src_install

    if option doc ; then
        edo pushd docs
        dodoc -r html
        edo popd
    fi

    if option java; then
        edo cd "${WORK}"/out/lib
        exeinto /usr/$(exhost --target)/lib/${PN}-${SLOT}
        doexe libmlt_java.so
    fi

    # Remove symlink created by the build system
    edo rm "${IMAGE}"/usr/$(exhost --target)/bin/melt

    alternatives_for mlt ${SLOT} ${SLOT} \
        /usr/$(exhost --target)/bin/melt melt-${SLOT}
}

